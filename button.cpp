#include "button.h"
#include <QDebug>
#include <QFont>
#include <QPalette>
#include <QColor>

MyButton::MyButton(QWidget *parent) : QPushButton(parent)
{
    setText("Wciśnij mnie");
//    setMouseTracking(true);
    //    setAttribute(Qt::WA_Hover);
}

void MyButton::paintEvent(QPaintEvent *ev)
{
    qDebug()<< ev->rect();
//    QRect geomet = this->geometry();
    QPainter paint(this);
//    paint.setPen(Qt::green);
    paint.drawRect(0,0,80,24);
//    paint.end();
//    paint.fillRect(geomet.x(), geomet.y(), geomet.width(), geomet.height(), QColor(Qt::green));
    paint.drawText(0,10, "Wciśnij mnie");
}

bool MyButton::event(QEvent *ev)
{
    if(ev->type()==QEvent::HoverEnter){

        QPalette p;
        p.setColor(QPalette::Button,QColor(Qt::green));
        setPalette(p);

        QFont font = this->font();
        font.setBold(true);
        this->setFont(font);

        qDebug()<<"Enter";
    }
    if(ev->type()==QEvent::HoverLeave){

        QPalette p;
        p.setColor(QPalette::Button,QColor(Qt::red));
        setPalette(p);

        QFont font = this->font();
        font.setBold(false);
        this->setFont(font);

        qDebug()<<"Leave";
    }
    return QPushButton::event(ev);
}
