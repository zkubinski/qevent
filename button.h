#ifndef BUTTON_H
#define BUTTON_H

#include <QPushButton>
#include <QObject>
#include <QWidget>
#include <QEvent>
#include <QHoverEvent>
#include <QPalette>
#include <QPainter>

class MyButton : public QPushButton
{
    Q_OBJECT

public:
    MyButton(QWidget *parent=nullptr);

private:
    QPalette backgroundButton;

    void buttonHover(QHoverEvent *ev);
    void paintEvent(QPaintEvent *ev) override;
    bool event(QEvent *) override;
};

#endif // BUTTON_H
