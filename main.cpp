#include "widget.h"
#include <QDebug>

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qDebug()<< a.palette();
    a.setStyle("Fusion");
    Widget w;
    w.show();
    return a.exec();
}
