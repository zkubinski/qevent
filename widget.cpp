#include "widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) : QWidget(parent)
{
    resize(200,100);
    button = new MyButton(this);
    QObject::connect(button, &QPushButton::clicked, this, &QWidget::close);
}

void Widget::closeEvent(QCloseEvent *event)
{
    event->accept();
    qDebug()<< "close";
}

Widget::~Widget()
{
}

